﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Windows.Forms;

using JWT;
using JWT.Algorithms;
using JWT.Serializers;

using Newtonsoft;

namespace JwtGenerator
{
    public partial class Form1 : Form
    {
        private static readonly double TimeToLiveInMinutes = 5;
        private static readonly RNGCryptoServiceProvider Random = new RNGCryptoServiceProvider();

        static Form1()
        {
            double ttl;

            if (double.TryParse(ConfigurationManager.AppSettings["timeToLiveInMinutes"], out ttl))
            {
                TimeToLiveInMinutes = ttl;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            var claims = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(textBoxPayload.Text);
            var dateTimeProvider = new UtcDateTimeProvider();
            var secret = ConfigurationManager.AppSettings["tadsClientSecret"];

            var now = dateTimeProvider.GetNow();

            var payload = new Dictionary<string, object>()
            {
                { "aud", ConfigurationManager.AppSettings["tadsClientId"] },
                { "exp", (int)(now.AddMinutes(TimeToLiveInMinutes) - JwtValidator.UnixEpoch).TotalSeconds },
                { "iat", (int)(now - JwtValidator.UnixEpoch).TotalSeconds },
                { "iss", ConfigurationManager.AppSettings["issuer"] },
                { "nonce", GenerateNonce(50) }
            };

            foreach (var pair in claims)
            {
                payload.Add(pair.Key, pair.Value);
            }

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, secret);

            textBoxToken.Text = token;
        }

        private string GenerateNonce(int length)
        {
            var data = new byte[length];

            Random.GetNonZeroBytes(data);

            return Convert.ToBase64String(data);
        }
    }
}
